# Photo Backup Script

I was a bit pissed of by the crappy usage when you want to copy files to your PC with the `File transfer`. ADB to the rescue!     
    
Backs up photos from an Android device using ADB, ensuring safe transfers.    
Creates device-specific folders to keep backups sorted.    
Attempts to ensure atomic file transfers for complete downloads (well, best-effort).    
Pulls photos in parallel for faster backups, limited by MAX_JOBS.    
    

## Prerequisites
Bash (Linux) WSL might work too, can not say.     
ADB (Android Debug Bridge): https://developer.android.com/tools/adb    
GNU Parallel: Install with your package manager (e.g., `sudo dnf install parallel`, `sudo apt update; sudo apt install     parallel`) source site https://www.gnu.org/software/parallel/    


## Usage

Execute the script from the directory where you'd like to store the backup.     

- The script will create a folder with the name of your device
- it will `ls` the content of the folder `/storage/emulated/0/DCIM/Camera/` as a filelist
- when there is a file with the same name on the local disk, skip and move to the next file
- when there is no file on the local disk with the same name as the remote file, it will download it to an tempoary dir.
- When finished, move the file to the device folder.

on an oldish phone you can check 1000 files in < 7s