#!/usr/bin/env bash

# Pull all photos and videos from your phone
# Uses the age old utility parallel to spawn 'n' adb pull commands at once

# Set maximum parallel jobs
MAX_JOBS=15

command -v adb > /dev/null 2>&1 || error "Please install adb first"
command -v parallel > /dev/null 2>&1 || error "Please install first parallel and execute it one time (parallel --citation)"
if ! test -w .; then
  echo -e "\033[0;31m[Error]\033[0m not able to create a folder in $PWD"; exit 1
fi
device_name=$(adb shell getprop ro.product.model | tr -d '\r') 
device_name="${device_name//[^a-zA-Z0-9]/_}"  # Replace non-alphanumeric with underscores
mkdir -p "${device_name}/tmp"

# Function to pull single file
pull_file() {
  remote_file="$1"
  device_name="$2"
  echo "${device_name}:${remote_file}: "
  if [ ! -f "${device_name}/${remote_file}" ]; then
    echo "Pulling: $remote_file"
    # make it atomic, its easy to end up with half files 
    adb pull "/storage/emulated/0/DCIM/Camera/$remote_file" "$device_name/tmp/$remote_file"
    mv "$device_name/tmp/$remote_file" "$device_name/$remote_file"
  fi
}
# Exportfunction to be used by GNU Parallel
export -f pull_file

# Get remote file list and process with GNU Parallel
adb shell ls /storage/emulated/0/DCIM/Camera/ | parallel -j "$MAX_JOBS" pull_file {} "${device_name}"