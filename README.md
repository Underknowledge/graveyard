# graveyard

## adb 
Android related. adb/resumable_DCIM_pull.sh lets you want to do a quick photo backup

## nix/shells/* 
Diffrent nix shells. Monitoring is quite useful when you want an quick prometheus/nodeexporter setup for a week only




## shellscripts
well, shellscripts 


## shellscripts/s3/s3-fancyshare-tui.py
    
S3 - `SigV4 pre-signed URLs` to create temp URLs to an item in a bucket. Read more [here](https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html)
    
lazy-to-use interface for generating pre-signed URLs in an S3 bucket.   
Keeps an reccord of generated URLs besides the script in a json file (`${scriptname//py/json}`) minimal poc @`s3-unsexyshare.py`

```bash
pip install boto3 npyscreen
python3 s3-fancyshare-tui.py 
```