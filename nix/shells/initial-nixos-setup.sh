#!/usr/bin/env bash
## sudo wipefs -a /dev/sda
# echo "nameserver 1.1.1.1" >> /etc/resolv.conf

disks_info=$(lsblk -P -o NAME,RM,SIZE,TYPE,FSTYPE)
disk_name=""

while IFS= read -r line; do
    # Check if the disk is non-removable (RM="0") and has no filesystem (FSTYPE="")
    if [[ $line == *RM=\"0\"* ]] && [[ $line == *FSTYPE=\"\"* ]]; then
        # gimme disk name
        temp_disk_name=$(echo "$line" | grep -o 'NAME="[^"]*' | cut -d\" -f2)
        
        # Check if the TYPE is "disk" to ensure it's not a partition or loop device
        if [[ $line == *TYPE=\"disk\"* ]]; then
            disk_name="/dev/$temp_disk_name"
            echo "Selected disk: $disk_name"
            # Break if the right disk is found
            break
        fi
    fi
done <<< "$disks_info"

if [[ -n $disk_name ]]; then
    # Use ls -l to find the disk by-id symlink
    disk_by_id=$(ls -l /dev/disk/by-id/ | grep $(basename $disk_name) | head -n 1 | awk '{print $9}')
    if [[ -n $disk_by_id ]]; then
        disk_by_id_path="/dev/disk/by-id/$disk_by_id"
        echo "Disk by-id path: $disk_by_id_path"
    else
        echo "Disk by-id not found for $disk_name"
        exit 1
    fi
else
    echo "No suitable disk found"
    exit 1
fi

DISKO_CFG="/tmp/disko-config.nix"
new_disk_by_id="/dev/disk/by-id/$disk_by_id"
curl https://raw.githubusercontent.com/nix-community/disko/master/example/hybrid.nix -o $DISKO_CFG

if [ ! -f "$DISKO_CFG" ]; then
    echo "Error: Required configuration file '$DISKO_CFG' not found. Is it DNS?"
    exit 1
fi

sed -i "s|device = \"/dev/disk/by-id/.*\"|device = \"$new_disk_by_id\"|g" $DISKO_CFG


sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko /tmp/disko-config.nix
nixos-generate-config --no-filesystems --root /mnt
mv /tmp/disko-config.nix /mnt/etc/nixos



CONFIG_FILE="/mnt/etc/nixos/configuration.nix"
if ! grep -q "./disko-config.nix" "$CONFIG_FILE"; then
  cp "$CONFIG_FILE" "$CONFIG_FILE.bak"
  # Use awk to insert lines after the pattern
  sed -i "/\.\/hardware-configuration.nix/a \ \ \ \ \ \"\${builtins.fetchTarball \"https://github.com/nix-community/disko/archive/master.tar.gz\"}/module.nix\"\n \ \ \ \ \ \.\/disko-config.nix" "$CONFIG_FILE"
  echo "Inserted disko-config.nix and fetchTarball line into $CONFIG_FILE."
else
  echo "disko-config.nix is already present in $CONFIG_FILE. No changes made."
fi




cat << EOF

check if /mnt/etc/nixos/configuration.nix looks like this:

---
imports =
 [ # Include the results of the hardware scan.
   ./hardware-configuration.nix
   "${builtins.fetchTarball "https://github.com/nix-community/disko/archive/master.tar.gz"}/module.nix"
   ./disko-config.nix
 ];


  services = {
    openssh = {
      enable = true;
      openFirewall = true;
      settings = {
          # PermitRootLogin can be set to "yes", "without-password", "prohibit-password", "no"
          PermitRootLogin = "yes"; 
          PasswordAuthentication = true; 
      };
    };
  };


---
you have to manually add the sshd config - at the moment no time to implement

nixos-install
reboot
EOF