let
  # Import the old version of nixpkgs for MinIO
  oldNixpkgs = import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/1732ee9120e43c1df33a33004315741d0173d0b2.tar.gz";
  }) {};

  # Import the latest version of nixpkgs for the other packages
  newNixpkgs = import <nixpkgs> {};

  # Use the old version of MinIO
  minio = oldNixpkgs.minio;

  # Use the latest versions of the other packages
  tmux = newNixpkgs.tmux;
  awscli = newNixpkgs.awscli;
  s3fs = newNixpkgs.s3fs;
  rclone = newNixpkgs.rclone;
in
  with newNixpkgs;
  stdenv.mkDerivation {
    name = "my-env";
    buildInputs = [ minio tmux awscli s3fs rclone ];

    shellHook = ''
      DATA_PATH=/opt/ZAP
      export HISTFILE=$HOME/.bash_history_minio

      export MINIO_ROOT_USER='Access'
      export MINIO_ROOT_PASSWORD='SecretSecret'

      cd $DATA_PATH || exit 1
      # RUN 
      #minio server $DATA_PATH
      tmux new-session -d -s minio-session "minio server $DATA_PATH"
      echo "MinIO server running in tmux session 'minio-session'. To attach to this session, run: tmux attach -t minio-session"

      # Prepare s3fs password file
      export S3FS_PASSWORD_FILE=$HOME/.bash_history_minio.s3fs_passwd
      echo "$MINIO_ROOT_USER:$MINIO_ROOT_PASSWORD" > $S3FS_PASSWORD_FILE
      chmod 600 $S3FS_PASSWORD_FILE
      # Create a new tmux pane for s3fs after a short delay
      tmux split-window -h -t minio-session
      tmux send-keys -t minio-session:0.1 "sleep 5; mkdir -p /tmp/s3-test/; s3fs mybucket /tmp/s3-test/ -o passwd_file=$S3FS_PASSWORD_FILE -o url=http://localhost:9000 -o use_path_request_style" C-m
      # /tmp/s3-test/
      tmux attach -t minio-session
    '';
  }
