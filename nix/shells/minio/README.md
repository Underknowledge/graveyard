# [WIP] MinIO NAS gateway

Not completly tested, but... 
Minio __had__ an `S3-NFS-Proxy` but the NAS gateway has been deprecated
not sure that it will work when I read https://github.com/minio/minio/discussions/15818  

https://blog.min.io/minio-gateway-migration/
```
We have deprecated MinIO Gateway. The product was initially developed to allow customers to use the S3 API to work with backends, such as NFS, Azure Blob and HDFS, that would not otherwise support it. The MinIO Gateway primarily acted as a translation layer for S3 API between the client and the backend so each backend could be written to in its native format.
```

This shell tries to get the latest working version up.