{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.wireguard-tools
    pkgs.tmux
    pkgs.wireguard-go
    pkgs.iproute
    pkgs.iputils
    pkgs.gawk
    pkgs.coreutils
  ];


  shellHook = ''
    WG_SESSION="wireguard_tunnel"
    SERVER_CONFIG="wg0-server-nix.conf"
    CLIENT_CONFIG="wg0-client-nix.conf"
    SERVER_PRIVATE_KEY=$(wg genkey)
    SERVER_PUBLIC_KEY=$(echo $SERVER_PRIVATE_KEY | wg pubkey)
    CLIENT_PRIVATE_KEY=$(wg genkey)
    CLIENT_PUBLIC_KEY=$(echo $CLIENT_PRIVATE_KEY | wg pubkey)

    # Dynamically get the server's IP address
    SERVER_IP=$(ip route get 1.2.3.4 | awk '{print $7}' | head -n 1)
    SERVER_ENDPOINT="$SERVER_IP:51820" # Assuming the default WireGuard port

    # IPs for server and client configuration
    SERVER_WG_IP="10.0.5.1/24"
    CLIENT_WG_IP="10.0.5.2/24"

    # Generate Server Config
    echo "[Interface]
PrivateKey = $SERVER_PRIVATE_KEY
Address = $SERVER_WG_IP
ListenPort = 51820

[Peer]
PublicKey = $CLIENT_PUBLIC_KEY
AllowedIPs = $CLIENT_WG_IP" > $SERVER_CONFIG

    # Generate Client Config
    echo "[Interface]
PrivateKey = $CLIENT_PRIVATE_KEY
Address = $CLIENT_WG_IP

[Peer]
PublicKey = $SERVER_PUBLIC_KEY
AllowedIPs = $SERVER_WG_IP
Endpoint = $SERVER_ENDPOINT
PersistentKeepalive = 25" > $CLIENT_CONFIG

    if tmux has-session -t $WG_SESSION 2>/dev/null; then
      echo "Session $WG_SESSION already exists. Attaching..."
      tmux attach -t $WG_SESSION
    else
      echo "Creating new tmux session $WG_SESSION."
      tmux new-session -d -s $WG_SESSION
      # Assuming the user has configured sudoers to allow passwordless execution of wg-quick
      tmux send-keys -t $WG_SESSION "sudo wg-quick up ./$SERVER_CONFIG" C-m
      tmux attach -t $WG_SESSION
    fi
  '';
}