## Userspace WireGuard Tunnel Setup in a Nix-Shell

The setup automates the creation of WireGuard configurations for both a server and a client
It runs the VPN tunnel in a tmux session. 

you might want to run `sudo wg-quick down wg0-server-nix` when youre finished