{ pkgs ? import <nixpkgs> {} }:

let
  prometheusConfig = ''
    global:
      scrape_interval: 15s

    scrape_configs:
      - job_name: 'node_exporter'
        static_configs:
          - targets: ['localhost:9100']
  '';
  tmuxConfig = ''
    # Enable mouse support
    set -g mouse on
    # clock mode
    setw -g clock-mode-colour colour1

    # copy mode
    setw -g mode-style 'fg=colour1 bg=colour18 bold'

    # pane borders
    set -g pane-border-style 'fg=colour1'
    set -g pane-active-border-style 'fg=colour3'

    # statusbar
    set -g status-position bottom
    set -g status-justify left
    set -g status-style 'fg=colour1'
    set -g status-right '#[fg=yellow]#(date "+%H:%M:%S %d-%b-%y")'
    set -g status-right-length 50
    set -g status-left-length 10

    setw -g window-status-current-style 'fg=colour0 bg=colour1 bold'
    setw -g window-status-current-format ' #I #W #F '

    setw -g window-status-style 'fg=colour1 dim'
    setw -g window-status-format ' #I #[fg=colour7]#W #[fg=colour1]#F '

    setw -g window-status-bell-style 'fg=colour2 bg=colour1 bold'
    # messages
    set -g message-style 'fg=colour2 bg=colour0 bold'
  '';
in
pkgs.mkShell {
  buildInputs = [ pkgs.tmux pkgs.prometheus-node-exporter pkgs.prometheus pkgs.grafana ];

  shellHook = ''
    export HISTFILE=$HOME/.bash_history_monitoringstack
    echo "${prometheusConfig}" > ./prometheus.yml
    echo "${tmuxConfig}" > .tmux.conf

    # Start tmux session with two panes
    if tmux has-session -t monitoring 2>/dev/null; then
      echo "Attaching to existing session 'monitoring'"
      tmux attach-session -t monitoring
    else
      echo "Creating new session 'monitoring'"
      tmux new-session -d -s monitoring -f .tmux.conf
      tmux split-window -h
      tmux send-keys -t monitoring:0.0 'node_exporter --collector.logind --collector.systemd --collector.meminfo --collector.processes --collector.interrupts --collector.tcpstat --web.listen-address 127.0.0.1:9100' C-m
      sleep 1
      tmux send-keys -t monitoring:0.1 'prometheus --config.file=./prometheus.yml --storage.tsdb.path=./data' C-m
      tmux attach-session -t monitoring
    fi
  '';
}
