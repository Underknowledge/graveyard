#!/bin/bash


if [ "$EUID" -ne 0 ]
  then error "Please run as root, we instal things here"
fi


# Destination directory
DEST_DIR="/usr/local/bin"
install_from_github() {
    # GitHub API URL for Age's latest release
    API_URL="https://api.github.com/repos/FiloSottile/age/releases/latest"

    # Query GitHub API for the latest release and extract the tarball URL
    echo "Querying GitHub API for the latest release..."
    LATEST_URL=$(curl -s $API_URL | jq -r '.assets[] | select(.name | contains("linux-amd64")).browser_download_url')

    # Extract filename
    FILE_NAME=$(basename $LATEST_URL)

    # Download Age
    echo "Downloading Age..."
    curl -L $LATEST_URL -o /tmp/$FILE_NAME

    # Extract and move to destination
    echo "Installing Age..."
    tar -xzf /tmp/$FILE_NAME -C /tmp
    sudo mv /tmp/age/age /tmp/age/age-keygen $DEST_DIR

    # Make executable
    sudo chmod +x $DEST_DIR/age $DEST_DIR/age-keygen

    # Clean up
    rm -rf /tmp/age*
    rm /tmp/$FILE_NAME

    echo "Age installation complete. Type 'age' or 'age-keygen' to use."
}

# Function to attempt Age installation using available package managers
install_with_package_manager() {
    # Define package manager commands and package names
    declare -A pkg_cmds=(
        ["apt-get"]="sudo apt-get update && sudo apt-get install -y age"
        ["yum"]="sudo yum install -y age"
        ["dnf"]="sudo dnf install -y age"
        ["brew"]="brew install age"
        ["pacman"]="sudo pacman -Sy age"
    )

    for pkg_manager in "${!pkg_cmds[@]}"; do
        if command -v $pkg_manager &> /dev/null; then
            echo "Attempting to install Age with $pkg_manager..."
            if eval "${pkg_cmds[$pkg_manager]}"; then
                echo "Age installed successfully with $pkg_manager."
                return 0
            else
                echo "Failed to install Age with $pkg_manager."
                # No return here to allow for other package managers to be tried
            fi
        fi
    done

    # If we reach this point, all attempts have failed
    echo "Failed to install Age with known package managers. Will attempt to install from GitHub."
    return 1
}

if command -v age &> /dev/null; then
    echo "Age is already installed."
    exit 0
fi


if ! install_with_package_manager; then
    install_from_github
fi