import boto3
from botocore.client import Config
from botocore.exceptions import ClientError
import datetime

endpoint_url = 
bucket_name = 
object_name = 
access_key = 
secret_key = 

session = boto3.Session(
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
)

client = session.client('s3', 
                        region_name='some-region', 
                        endpoint_url=endpoint_url,
                        config=Config(signature_version='s3v4'))

try:
    response = client.generate_presigned_url('get_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name},
                                             ExpiresIn=3600) #secconds
    print(response)
except ClientError as e:
    print(e)