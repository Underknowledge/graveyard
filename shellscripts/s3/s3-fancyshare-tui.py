import boto3
from botocore.exceptions import ClientError
from botocore.config import Config
import datetime
import json
import npyscreen
import re
import os
from datetime import timedelta

# load config.py and assign them to the old variables I defined. It would have taken the time I needed to document to change it
import config
endpoint_url = config.endpoint_url
bucket_name = config.bucket_name
access_key = config.access_key
secret_key = config.secret_key
region_name = config.region_name

generated_url = None
shared_files = []
generated_url = None
selected_file = None
end_time = None

script_path = os.path.abspath(__file__)  # Absolute path of the script
script_dir = os.path.dirname(script_path)  # Directory
log_filename = os.path.splitext(os.path.basename(script_path))[0] + '.json'  # Change extension to .json
log_file_path = os.path.join(script_dir, log_filename)  # Full path log

try:
    with open(log_file_path, 'r') as file:
        shared_files_data = json.load(file)
        if isinstance(shared_files_data, list):
            shared_files = shared_files_data
        else:
            print("The json file is not in the expected format. It should be a list.")
except FileNotFoundError:
    shared_files = []
except json.JSONDecodeError:
    print("The json file")

session = boto3.Session(
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
)

# Create a client with SigV4 configuration
client = session.client('s3', region_name=region_name, endpoint_url=endpoint_url, config=Config(signature_version='s3v4'))

# list files in a bucket
def list_files(bucket):
    try:
        response = client.list_objects_v2(Bucket=bucket)
        return [item['Key'] for item in response.get('Contents', [])]
    except ClientError as e:
        print(e)
        return []

# generate a presigned URL
def generate_presigned_url(bucket, object_name, expiration):
    try:
        response = client.generate_presigned_url('get_object',
                                                 Params={'Bucket': bucket,
                                                         'Key': object_name},
                                                 ExpiresIn=expiration)
        return response
    except ClientError as e:
        print(e)
        return None

def parse_time_string(time_str):
    total_seconds = 0
    for unit, value in re.findall(r'(\d+)([smhd])', time_str):
        if value == 's':
            total_seconds += int(unit)
        elif value == 'm':
            total_seconds += int(unit) * 60
        elif value == 'h':
            total_seconds += int(unit) * 3600
        elif value == 'd':
            total_seconds += int(unit) * 86400
    return timedelta(seconds=total_seconds)

class S3FileSelector(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm('MAIN', FileSelectionForm, name='Select File from S3')

class FileSelectionForm(npyscreen.ActionForm):
    def create(self):
        self.file_list = self.add(npyscreen.TitleSelectOne, max_height=7, name='Files', values=list_files(bucket_name))
        self.duration = self.add(npyscreen.TitleText, name='Duration (e.g., 1m 3d 55s)', value='24h')

    def on_ok(self):
        global generated_url, selected_file, end_time  # Refer to the global variable

        selected_file = self.file_list.get_selected_objects()[0]
        duration_str = self.duration.value or '24h'
        duration = parse_time_string(duration_str).total_seconds()
        url = generate_presigned_url(bucket_name, selected_file, int(duration))

        if url:
            generated_url = url
            current_time = datetime.datetime.now()
            end_time = current_time + timedelta(seconds=int(duration))
            log_entry = {
                'file': selected_file,
                'url': url,
                'start_date': current_time.strftime('%Y-%m-%d %H:%M:%S'),
                'end_date': end_time.strftime('%Y-%m-%d %H:%M:%S')
            }

            # log
            shared_files.append(log_entry)
            with open(log_file_path, 'w') as file:
                json.dump(shared_files, file, indent=4)
        else:
            generated_url = "Failed to generate URL."

        self.parentApp.setNextForm(None) # Exit the TUI after setting the URL

    def on_cancel(self):
        global generated_url
        generated_url = None
        self.parentApp.setNextForm(None)

# Run the TUI app
if __name__ == '__main__':
    app = S3FileSelector()
    app.run()
    if generated_url:
        print("\nfile: " + selected_file)
        print("end_date: " + end_time.strftime('%Y-%m-%d %H:%M:%S'))
        print("Generated URL: \n  " + generated_url + "\n" )