import configparser
import glob
import shutil
import os
import sqlite3
import sys
from datetime import datetime, timedelta


SEPARATE = True  # Set this to False if you don't want separators

profiles_ini_path = os.path.expanduser('~/.mozilla/firefox/profiles.ini')


def has_session_backup_files(profile_path):
    session_backup_path = os.path.join(profile_path, 'sessionstore-backups')
    session_files = glob.glob(os.path.join(session_backup_path, 'recovery.jsonlz4'))
    return len(session_files) > 0

def get_firefox_profiles(profiles_ini):
    config = configparser.ConfigParser()
    config.read(profiles_ini)
    profiles = {}
    for section in config.sections():
        if section.startswith('Profile'):
            name = config[section].get('Name')
            is_relative = config[section].get('IsRelative') == '1'
            path = config[section].get('Path')
            if is_relative:
                path = os.path.join(os.path.dirname(profiles_ini), path)
            if has_session_backup_files(path):
                profiles[name] = path
    return profiles

def choose_profile(profiles):
    print("Available Firefox profiles:")
    for i, (name, _) in enumerate(profiles.items(), start=1):
        print(f"{i}. {name}")
    choice = input("Select a profile by number or name: ")
    if choice.isdigit() and 0 < int(choice) <= len(profiles):
        selected = list(profiles.items())[int(choice) - 1][0]
    elif choice in profiles:
        selected = choice
    else:
        print("Invalid selection. Please try again.")
        return choose_profile(profiles)
    return profiles[selected]

def extract_history_to_markdown(profile_path, date_str, output_md_file):
    db_path = os.path.join(profile_path, "places.sqlite")
    temp_db_path = "/tmp/places.sqlite"
    shutil.copy2(db_path, temp_db_path)

    start_epoch, end_epoch = date_to_epoch(date_str)
    
    conn = sqlite3.connect(temp_db_path)
    cur = conn.cursor()

    query = """SELECT url, title, datetime(last_visit_date / 1000000, 'unixepoch') as visit_date
               FROM moz_places
               WHERE last_visit_date BETWEEN ? AND ?
               ORDER BY last_visit_date"""

    cur.execute(query, (start_epoch, end_epoch))
    visits = cur.fetchall()

    if not visits:
        print("No history found for the given date.")
        return


    visits = [(url, title, datetime.strptime(visit_date, "%Y-%m-%d %H:%M:%S")) for url, title, visit_date in visits]
    last_visit_time = visits[-1][2]  

    with open(output_md_file, 'w') as md_file:
        md_file.write(f"| {last_visit_time.strftime('%Y-%m-%d ')} | URL | Title |\n")
        md_file.write("| --- | --- | --- |\n")

        current_hour_marker = visits[0][2].replace(minute=0, second=0, microsecond=0)
        if current_hour_marker.hour % 2 != 0: 
            current_hour_marker += timedelta(hours=1)

        for url, title, visit_time in visits:
            # Write hour markers before each visit if they fall within the hour
            while current_hour_marker <= visit_time:
                if SEPARATE:
                    if current_hour_marker.hour % 2 == 0:
                        md_file.write(f"| **{current_hour_marker.strftime('%H:00')}** | --- | --- |\n")
                current_hour_marker += timedelta(hours=1)

            last_visit_time = visit_time

            friendly_name = url.split("//")[-1].split("/")[0]
            # title_escaped = title.replace("|", "\\|") 
            if title is None:
                title_escaped = friendly_name
            else:
                title_escaped = title.replace("|", "\\|") 
            DEBUG = True
            if not DEBUG:
                md_file.write(f"| {visit_time.strftime('%H:%M')} | [{friendly_name}]({url}) | {title_escaped} |\n")
            else:
                md_file.write(f"| {visit_time.strftime("%Y-%m-%d %H:%M:%S")} | [{friendly_name}]({url}) | {title_escaped} |\n")

        if last_visit_time.hour % 2 == 0 and last_visit_time.minute == 0 and last_visit_time.second == 0:
            md_file.write(f"| --- | **{last_visit_time.strftime('%H:00')}** | --- |\n")

    print(f"History has been written to {output_md_file}")

    conn.close()



# Function to convert input date to the format stored in places.sqlite
def date_to_epoch(date_str):
    formats = ["%Y-%m-%d", "%y-%m-%d"] # supportet date formats
    
    for date_format in formats:
        try:
            start_date = datetime.strptime(date_str, date_format)
            end_date = start_date + timedelta(days=1)
            start_epoch = int(start_date.timestamp() * 1e6)
            end_epoch = int(end_date.timestamp() * 1e6)
            return start_epoch, end_epoch
        except ValueError:
            continue  # Try next
    raise ValueError("Date format does not match supported formats: 'yyyy-mm-dd' or 'yy-mm-dd'")


def list_dates_for_selection():
    from datetime import datetime, timedelta  

    today = datetime.today()
    start_of_current_month = today.replace(day=1)
    last_month = start_of_current_month - timedelta(days=1)
    start_of_last_month = last_month.replace(day=1)

    dates = [start_of_last_month + timedelta(days=x) for x in range((today - start_of_last_month).days)]
    dates_str = [date.strftime("%Y-%m-%d") for date in dates]

    max_index_length = len(str(len(dates_str)))

    previous_month = None
    print() 
    for i, date in enumerate(dates_str, start=1):
        current_month = dates[i-1].month 
        if previous_month is not None and current_month != previous_month:
            print()  
        print(f"{i:>{max_index_length}}. {date}")  # Right-align the index
        previous_month = current_month  
    print() 
    choice = int(input("Select a date by number: ")) - 1
    if 0 <= choice < len(dates_str):
        return dates_str[choice]
    else:
        print("Invalid selection. Please try again.")
        return list_dates_for_selection()


profiles = get_firefox_profiles(profiles_ini_path)
if profiles:
    if len(profiles) == 1:
        selected_profile_path = next(iter(profiles.values()))
        print(f"Only one profile found. Using profile: {next(iter(profiles.keys()))}")
    else:
        selected_profile_path = choose_profile(profiles)
    
    if len(sys.argv) != 2:
        print("No date provided. Please select a date from the list below:")
        date_str = list_dates_for_selection()
    else:
        date_str = sys.argv[1]
    
    output_md_file = 'firefox_history.md'
    if os.path.exists(output_md_file):
        os.remove(output_md_file)
    extract_history_to_markdown(selected_profile_path, date_str, output_md_file)
    os.remove("/tmp/places.sqlite")
else:
    print("No profiles with session backup files found.")