import json
import lz4.block

# python .print_recoveryJson.py | gron | grep -v '.image' | grep -v '.csp' | grep -v 'structuredCloneState' | grep -v triggeringPrincipal_base64 | gron --ungron | jq

# Path to the recovery.jsonlz4 file
recovery_file_path = '/home/user/.mozilla/firefox/kd1tc14l.default-release/sessionstore-backups/recovery.jsonlz4'

def pretty_print_recovery_file(file_path):
    try:
        with open(file_path, 'rb') as file:
            file.read(8)  # Skip the Mozilla header
            data = file.read()
            decompressed_data = lz4.block.decompress(data)
            session_data = json.loads(decompressed_data)
            print(json.dumps(session_data, indent=4, sort_keys=True))
    except Exception as e:
        print(f"An error occurred: {e}")

pretty_print_recovery_file(recovery_file_path)