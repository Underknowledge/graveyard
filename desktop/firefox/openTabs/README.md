## Firefox Session URL Extracting
 
Ever find yourself buried under a mountain of research tabs in Firefox?     
Me too. I used to lean heavily on Zotero but have recently switched to Logseq. The biggest headache? Moving all those tabs over without manually copying and pasting each URL.

That's where a bit of Python magic comes in.

### `openTabs.py`

Sneaks into Firefox's session backup files, grabs all the URLs from your jungle of tabs, and neatly tucks them into a Markdown file     
Useful for users looking to backup or share their currently open tabs. Works even on different Firefox profiles.
    

    
When you just want to look into the file use `.print_recoveryJson.py`    
This Script interact with Firefox's session data, and "pretty-printing" the contents of Firefox's recovery.jsonlz4 session backup files. (you have to manually specify the path)