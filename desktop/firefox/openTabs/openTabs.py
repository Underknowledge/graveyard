import configparser
import json
import os
import glob
import lz4.block


# profiles.ini file, would be interesting if this works in Windows(I guess no)
profiles_ini_path = os.path.expanduser('~/.mozilla/firefox/profiles.ini')

# check if session backup files exist for a profile
def has_session_backup_files(profile_path):
    session_backup_path = os.path.join(profile_path, 'sessionstore-backups')
    session_files = glob.glob(os.path.join(session_backup_path, 'recovery.jsonlz4'))
    return len(session_files) > 0

# parse profiles.ini, return dictionary
def get_firefox_profiles(profiles_ini):
    config = configparser.ConfigParser()
    config.read(profiles_ini)
    profiles = {}
    for section in config.sections():
        if section.startswith('Profile'):
            name = config[section].get('Name')
            is_relative = config[section].get('IsRelative') == '1'
            path = config[section].get('Path')
            if is_relative:
                path = os.path.join(os.path.dirname(profiles_ini), path)
            if has_session_backup_files(path):
                profiles[name] = path
    return profiles

# choose a profile
def choose_profile(profiles):
    print("Available Firefox profiles:")
    for i, (name, _) in enumerate(profiles.items(), start=1):
        print(f"{i}. {name}")
    choice = input("Select a profile by number or name: ")
    if choice.isdigit() and 0 < int(choice) <= len(profiles):
        selected = list(profiles.items())[int(choice) - 1][0]
    elif choice in profiles:
        selected = choice
    else:
        print("Invalid selection. Please try again.")
        return choose_profile(profiles)
    return profiles[selected]

# extract URLs from the sessionstore-backups and write to Markdown
def extract_urls_to_markdown(profile_path, output_md_file):
    session_backup_path = os.path.join(profile_path, 'sessionstore-backups')
    latest_session_file = max(glob.glob(os.path.join(session_backup_path, 'recovery.jsonlz4')), key=os.path.getctime, default=None)
    
    if latest_session_file:
        print(f"Extracting tabs from session file: {latest_session_file}")
        with open(latest_session_file, 'rb') as file:
            file.read(8)  # Skip Mozilla header
            data = file.read()
            decompressed_data = lz4.block.decompress(data)
            session_data = json.loads(decompressed_data)
            
            with open(output_md_file, 'w') as md_file:
                for window in session_data.get('windows', []):
                    for tab in window.get('tabs', []):
                        i = tab.get('index') - 1
                        url = tab.get('entries', [{}])[i].get('url')
                        title = tab.get('entries', [{}])[i].get('title', url)
                        if url:
                            md_file.write(f"- [{title}]({url})\n")
        print(f"URLs have been written to {output_md_file}")
    else:
        print("No session backup file found.")

profiles = get_firefox_profiles(profiles_ini_path)
if profiles:
    selected_profile_path = choose_profile(profiles)
    output_md_file = 'open_firefox_tabs.md'
    extract_urls_to_markdown(selected_profile_path, output_md_file)
else:
    print("No profiles with session backup files found.")
